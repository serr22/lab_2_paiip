package dvl.srg.lab2.comparator;

import java.util.Comparator;

import dvl.srg.lab2.beans.RowItem;

/**
 * Created by Admin on 10/25/2015.
 */
public class RowItemComparator implements Comparator<RowItem> {

    private static boolean SORTED = true;

    public RowItemComparator (boolean sort) {
        this.SORTED = sort;
    }

    @Override
    public int compare(RowItem lhs, RowItem rhs) {
        if (SORTED) {
            return lhs.getTitle().compareTo(rhs.getTitle());
        }
        return rhs.getTitle().compareTo(lhs.getTitle());
    }
}
