package dvl.srg.lab2.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;

import java.io.File;
import java.io.IOException;

import dvl.srg.lab2.adapters.CustomBaseAdapter;
import dvl.srg.lab2.beans.RowItem;
import dvl.srg.lab2.comparator.RowItemComparator;
import dvl.srg.lab2.xml.XMLHandler;
import lab2.srg.dvl.lab_2_paiip.R;


public class MainActivity extends Activity implements OnItemClickListener, OnItemLongClickListener {

    private static final String TAG = "LogTag";
    private static final String FILE_NAME = "list.xml";

    private static boolean SORTED = false;

    public static int ADD_ITEM = 1;
    public static int VIEW_ITEM = 2;

    private File xmlfile;
    private XMLHandler xmlHandler;

    private CustomBaseAdapter adapter;

    private ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate MainActivity --> start...");
        setContentView(R.layout.activity_main);

        createStoreXMLFile();
        xmlHandler = new XMLHandler(xmlfile);
        Log.d(TAG, " parsare JDOM xml");
        xmlHandler.parseXMLAndStoreIt();

        Log.d(TAG, "create Adapter --> start... : " + xmlHandler.getItems());
        adapter = new CustomBaseAdapter(this, R.id.list , xmlHandler.getItems());

        listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_addItem) {
            Intent intent = new Intent(this, AddItemActivity.class);
            startActivityForResult(intent, ADD_ITEM);
            return true;
        }
        if (item.getItemId() == R.id.action_sort) {
            SORTED = !SORTED;
            adapter.sort(new RowItemComparator(SORTED));
            adapter.notifyDataSetChanged();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            xmlHandler.writeDataToXML();
        } catch (IOException e) {
            Log.d(TAG, "onDestroy MainActivity eroare la salvare in xml", e);
        }
    }

    private boolean createStoreXMLFile () {
        xmlfile  = new File (getFilesDir().getAbsolutePath() + "/" + FILE_NAME);
        if (!xmlfile.exists()) {
            try {
                xmlfile.createNewFile();
                return true;
            } catch (IOException e) {
                Log.d(TAG, "createStoreXMLFile MainActivity eroare la creare fisier xml", e);
            }
       }
        return false;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_ITEM) {
            if (resultCode == RESULT_OK) {
                xmlHandler.addItem((RowItem) data.getSerializableExtra("item"));
                adapter.notifyDataSetChanged();
            }
        }

        if (requestCode == VIEW_ITEM) {
            if (resultCode == RESULT_OK) {
                xmlHandler.updateItems((RowItem) data.getSerializableExtra("item"),
                        data.getIntExtra("position", 0));
                adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(this, ViewItemActivity.class);
        intent.putExtra("item", xmlHandler.getItem(position));
        intent.putExtra("position", position);
        startActivityForResult(intent, VIEW_ITEM);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d(TAG, "press onItemLongClick --> ...");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Delete Item?");
        final int pos = position;
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                xmlHandler.removeItem(pos);
                adapter.notifyDataSetChanged();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        builder.show();
        return false;
    }
}
