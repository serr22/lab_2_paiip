package dvl.srg.lab2.activities;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import dvl.srg.lab2.beans.RowItem;
import lab2.srg.dvl.lab_2_paiip.R;


public class AddItemActivity extends Activity {
    private static final int SELECT_PHOTO = 1;

    Intent getIntent = null;

    ImageView pictureEdit = null;
    EditText titleEdit = null;
    EditText descEdit = null;

    RowItem itemRow = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_list_item);
        // Show the Up button in the action bar.
        setupActionBar();

        pictureEdit = (ImageView) findViewById(R.id.pictureEdit);
        titleEdit = (EditText) findViewById(R.id.titleEdit);
        descEdit = (EditText) findViewById(R.id.descEdit);

        getIntent = getIntent();

        if (getIntent.getIntExtra("requestCode", 0) == 3) {
            itemRow = (RowItem) getIntent.getSerializableExtra("item");

            String pictureAdress = itemRow.getImagePath();
            if (!pictureAdress.isEmpty()) {
                Uri selectedImage = Uri.parse(pictureAdress);
                pictureEdit.setImageURI(selectedImage);
            }
            titleEdit.setText(itemRow.getTitle());
            descEdit.setText(itemRow.getDesc());

        } else {
            itemRow = new RowItem();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_item, menu);
        return true;
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setupActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.action_save:
                if (titleEdit.getText().toString().isEmpty() ||
                        descEdit.getText().toString().isEmpty()) {
                    Toast.makeText(this, "Write title and description", Toast.LENGTH_LONG).show();
                    return true;
                }

                itemRow.setTitle(titleEdit.getText().toString());
                itemRow.setDesc(descEdit.getText().toString());

                getIntent().putExtra("item", itemRow);
                setResult(RESULT_OK, getIntent());
                finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClick(View view) {
        Intent photoPick = new Intent(Intent.ACTION_PICK);
        photoPick.setType("image/*");
        startActivityForResult(photoPick, SELECT_PHOTO);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_PHOTO) {
            if (resultCode == RESULT_OK) {
                Uri selectedImage = data.getData();
                if (!selectedImage.toString().isEmpty()) {
                    itemRow.setImagePath(selectedImage.toString());
                    pictureEdit.setImageURI(selectedImage);
                }
            }
        }
    }
}
