package dvl.srg.lab2.activities;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import dvl.srg.lab2.beans.RowItem;
import lab2.srg.dvl.lab_2_paiip.R;

public class ViewItemActivity extends Activity {

    public static int EDIT_ITEM = 3;

    private Intent getIntent = null;
    private ImageView pictureView = null;
    private TextView titleView = null;
    private TextView descView = null;

    private RowItem rowItem = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_item);
        // Show the Up button in the action bar.
        setupActionBar();

        pictureView = (ImageView) findViewById(R.id.pictureView);
        titleView = (TextView) findViewById(R.id.titleView);
        descView = (TextView) findViewById(R.id.descView);

        getIntent = getIntent();
        rowItem = (RowItem) getIntent.getSerializableExtra("item");

        String pictureAdress = rowItem.getImagePath();
        if (!pictureAdress.isEmpty()) {
            pictureView.setImageURI(Uri.parse(pictureAdress));
        }
        titleView.setText(rowItem.getTitle());
        descView.setText(rowItem.getDesc());
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setupActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_view_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;

            case R.id.action_edit:
                Intent intent = new Intent(this, AddItemActivity.class);
                intent.putExtra("item", rowItem);
                intent.putExtra("requestCode", EDIT_ITEM);
                startActivityForResult(intent, EDIT_ITEM);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == EDIT_ITEM) {
            if (resultCode == RESULT_OK) {

                rowItem = (RowItem) data.getSerializableExtra("item");

                String pictureAdress = rowItem.getImagePath();
                if (!pictureAdress.isEmpty()) {
                    pictureView.setImageURI(Uri.parse(pictureAdress));
                }
                titleView.setText(rowItem.getTitle());
                descView.setText(rowItem.getDesc());

                getIntent.putExtra("item", rowItem);
                setResult(RESULT_OK, getIntent());
            }
        }
    }
}
