package dvl.srg.lab2.xml;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;


import dvl.srg.lab2.beans.RowItem;

public class XMLHandler {
    private ArrayList<RowItem> dates;
    private File xmlFile;

    public XMLHandler(File xmlFile) {
        this.xmlFile = xmlFile;
        dates = new ArrayList<RowItem>();
    }

    public void parseXMLAndStoreIt() {
        try {
            SAXBuilder saxBuilder = new SAXBuilder();
            Document document = saxBuilder.build(xmlFile);

            Element itemElement = document.getRootElement();

            List<Element> itemsList = itemElement.getChildren();

            for (int temp = 0; temp < itemsList.size(); temp++) {
                Element item = itemsList.get(temp);

                String imagePath = item.getChild("imagePath").getText();
                String title = item.getChild("title").getText();
                String desc = item.getChild("desc").getText();
                dates.add(new RowItem(imagePath, title, desc));
            }
        } catch (JDOMException e) {
        } catch (IOException e) {
        }
    }

    public void writeDataToXML() throws IOException {
        OutputStream outputStream = null;
        try {
            //root element
            File newF = new File(xmlFile.getAbsolutePath());
            Element itemsElement = new Element("items");
            Document doc = new Document(itemsElement);

            for (int i = 0; i < dates.size(); ++i) {
                //supercars element
                Element itemElement = new Element("item");

                Element imagePath = new Element("imagePath");
                imagePath.setText(dates.get(i).getImagePath());

                Element titleElement = new Element("title");
                titleElement.setText(dates.get(i).getTitle());

                Element descElement = new Element("desc");
                descElement.setText(dates.get(i).getDesc());

                itemElement.addContent(imagePath);
                itemElement.addContent(titleElement);
                itemElement.addContent(descElement);

                doc.getRootElement().addContent(itemElement);
            }
            XMLOutputter xmlOutput = new XMLOutputter();
            xmlOutput.setFormat(Format.getPrettyFormat());
            outputStream = new BufferedOutputStream(new FileOutputStream(newF));
            xmlOutput.output(doc, outputStream);

        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public List<RowItem> getItems() {
        return dates;
    }

    public void addItem(RowItem item) {
        dates.add(item);
    }

    public RowItem getItem(int position) {
        return dates.get(position);
    }

    public void removeItem(int posistion) {
        dates.remove(posistion);
    }

    public void updateItems(RowItem item, int position) {
        dates.set(position, item);
    }
}

