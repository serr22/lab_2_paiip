package dvl.srg.lab2.adapters;


import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import dvl.srg.lab2.beans.RowItem;
import lab2.srg.dvl.lab_2_paiip.R;

public class CustomBaseAdapter extends ArrayAdapter<RowItem> {
    Context context;
    List<RowItem> rowItems;

    public CustomBaseAdapter(Context context, int resource, List<RowItem> items) {
        super(context, resource, items);
        this.context = context;
        this.rowItems = items;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        View rowView = mInflater.inflate(R.layout.list_item, parent, false);
        TextView textTitle = (TextView) rowView.findViewById(R.id.title);
        TextView textDesc = (TextView) rowView.findViewById(R.id.desc);
        ImageView picture = (ImageView) rowView.findViewById(R.id.icon);

        RowItem rowItem = rowItems.get(position);

        textTitle.setText(rowItem.getTitle());
        textDesc.setText(rowItem.getDesc());

        String pictureAdress = rowItem.getImagePath();
        if (!pictureAdress.isEmpty()) {
            picture.setImageURI(Uri.parse(pictureAdress));
        }
        rowItem = null;
        return rowView;
    }
}
